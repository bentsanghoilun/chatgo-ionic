import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { State } from "ionicons/dist/types/stencil-public-runtime";
import { RootState } from "../store";
import { InitData } from "../types/InitData";
import CheckLocal from "../../components/Classes/CheckLocal";


const isWeb = window.self !== window.top;
// get search query from url to get init autoAuth params
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const initEventKey = urlParams.get('eventKey');
const initIsAutoAuth = urlParams.get('isAutoAuth') && parseInt(String(urlParams.get('isAutoAuth'))) === 1 ? true : false;

const initialState: InitData = {
    eventKey: isWeb ? String(initEventKey) : String(process.env.REACT_APP_EVENT_TOKEN) || '',
    userToken: initIsAutoAuth || !CheckLocal() ? '' : localStorage.getItem('token') || '',
    firebaseToken: initIsAutoAuth || !CheckLocal() ? '' : localStorage.getItem('firebase_token') || '',
    dmId: null,
    isAutoAuth: initIsAutoAuth,
    event_name: null,
    themeColors: null,
    mode: null
}

export const initDataSlice = createSlice({
    name: 'initData',
    initialState,
    reducers: {
        setDmId: (state, action: PayloadAction<string>) => {
            state.dmId = action.payload
        },
        setIsAutoAuth: (state, action: PayloadAction<boolean>) => {
            state.isAutoAuth = action.payload
        },
        setEventName: (state, action: PayloadAction<string>) => {
            state.event_name = action.payload
        },
        setMode: (state, action: PayloadAction<string>) => {
            state.mode = action.payload
        }
    }
});

export const selectDmId = (state: RootState) => state.initData.dmId;
export const selectIsAutoAuth = (state: RootState) => state.initData.isAutoAuth;
export const selectEventName = (state: RootState) => state.initData.event_name;
export const selectMode = (state: RootState) => state.initData.mode;

export const { setDmId, setIsAutoAuth, setEventName, setMode } = initDataSlice.actions;

export default initDataSlice.reducer;