import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { EventFilter } from "../../models/event";
import { RootState } from "../store";
import { EventToggles } from '../types/eventToggles';

interface EventInterface {
    toggles: Array<any>;
    filters: {
        chat: string;
    };
}

const initialState: EventInterface = {
    toggles: [],
    filters: {
        chat: '',
    },
}

export const eventSlice = createSlice({
    name: "events",
    initialState,
    reducers: {
        updateEventToggles: (state, action: PayloadAction<{ [key: string]: number }>) => {
            state.toggles = { ...state.toggles, ...action.payload };
        },
        updateFilters: (state, action: PayloadAction<string>) => {
            state.filters.chat = action.payload;
        }
    },
});

export const selectEventToggles = (state: RootState) => (state.events.toggles as EventToggles);
export const selectChatFilters = (state: RootState) => state.events.filters.chat

export const { updateEventToggles, updateFilters } = eventSlice.actions;

export default eventSlice.reducer;