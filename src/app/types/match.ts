import { User } from "../../pages/Auth/types";

export interface Match {
    match: Number;
    user: User
}

export interface Matches {
    [key: string]: Array<Match>;
}