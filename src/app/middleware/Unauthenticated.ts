import { Middleware, isRejectedWithValue } from '@reduxjs/toolkit';
import { userLogout } from '../firebase';

export const unauthenticatedMiddleware: Middleware = () => (next) => (action) => {

    if (isRejectedWithValue(action) && action.payload.status === 403) {
        // console.log('middleware:: auth rejected with 403, logging out');
        userLogout();
    }
  
    return next(action);
}