import { fetchBaseQuery } from '@reduxjs/toolkit/query'
import { RootState } from "./store";
import { store } from './store';

const eventToken = store.getState().auth.event_key;

export const chatgoBaseQuery = fetchBaseQuery({
    baseUrl: process.env.REACT_APP_CHATGO_API,
    prepareHeaders: (headers, { getState }) => {
        const token: string  = (getState() as RootState).auth.token;
        if (token.length > 0) {
            headers.set("authentication", `Bearer ${token}`);
        }

        headers.set("accept", "application/json");

        return headers;
    }
});