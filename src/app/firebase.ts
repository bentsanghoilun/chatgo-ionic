
import { initializeApp, getApps, getApp } from "firebase/app"
import { getFirestore, doc, getDoc, onSnapshot, query, collection, orderBy, where, limit, limitToLast, endBefore, Timestamp, writeBatch, getDocs, Unsubscribe, updateDoc, Firestore } from "firebase/firestore"
import { store } from './store';
import { logout } from '../pages/Auth/authSlice';
import { setUser, setUserAnswers, setUserMatchmaking, setUserSettings, updateUserStatus, selectUser, setUserTimestamps, setUserId } from './slices/userSlice'
import { getAuth, signInWithCustomToken, signOut, browserLocalPersistence, inMemoryPersistence, initializeAuth } from "firebase/auth";
import { User } from "../pages/Auth/types";
import { UserAnswer, UserSettings } from "./types/user";
import { Question } from "./types/questions";
import { setQuestions } from "./slices/questionSlice";
import { Match } from "./types/match";
import { setMatches } from "./slices/matchSlice";
import { Room } from "./types/rooms";
import { addRoom, setRoomCount, incrementCount, deleteRoom, deleteMessage, addMessageBulk, addRoomBulk, resetRoomSlice } from "./slices/roomSlice";
import { getDatabase, ref, onValue, onDisconnect, set } from "firebase/database";
import { Message, MessagePayload } from "../models/message";
import { checkDMExists, resetGroupSlice, setGroup, setGroupCount, updateGroupMessage, updateGroupUsers } from "./slices/groupSlice";
import { Group } from "./types/group";
import { getFunctions, httpsCallable } from 'firebase/functions';
import { SplashScreen } from '@capacitor/splash-screen';
import { selectChatFilters, updateEventToggles, updateFilters } from "./slices/eventSlice";
import { EventFilter } from "../models/event";
import { PostMessageToParent } from "../components/Classes/PostMessage";
import CheckLocal from "../components/Classes/CheckLocal";
import { Storage } from '@capacitor/storage';
import videoCallStatus from "./enums/videoCallStatus";
import { DolbyLoginResponse } from "./types/videoCall";

const isWeb = window.self !== window.top;

const unsubscribers: Array<Unsubscribe> = [];

const unsubscriberss: { [key: string]: Unsubscribe | undefined } = {
    'QUESTION_LISTENER': undefined,
    'MATCHES_LISTENER': undefined,
    'FILTER_LISTENER': undefined,
    'ROOM_LISTENER': undefined,
    'GROUP_LISTENER': undefined,
};

const setStorageUser = async (user: any) => {
    await Storage.set({
        key: 'user',
        value: JSON.stringify(user)
    })
}

const getStorageUser = async () => {
    const { value } = await Storage.get({ key: 'user' });
    console.log('getUser', JSON.parse(String(value)));
    return JSON.parse(String(value));
}

const removeStorageUser = async () => {
    await Storage.remove({ key: 'user' });
}

export const boostrapFirebase = async () => {

    if (getApps().length === 0 || unsubscribers.length === 0) {

        await SplashScreen.show({
            autoHide: false
        });

        initializeApp({
            apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
            authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
            projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
            databaseURL: process.env.REACT_APP_FIRESTORE_URL,
        });

        const isAutoAuth = store.getState().initData.isAutoAuth;
        const authPersistence = !isWeb ? browserLocalPersistence : isAutoAuth || !CheckLocal() ? inMemoryPersistence : browserLocalPersistence;
        const fToken: string = store.getState().auth.firebase_token;
        let auth = initializeAuth(getApp(), {
            persistence: authPersistence
        })

        auth.onAuthStateChanged(user => {
            if (user) {
                const existingUserSetup = async (user: any) => {
                    await setUpListeners(user);
                    await SplashScreen.hide();
                }

                existingUserSetup(user);
            } else {
                const newSignInSetup = async () => {
                    if (CheckLocal() && !isAutoAuth) {
                        //if is Mobile, check user storage first
                        const storageUser = await getStorageUser();
                        if (storageUser) {
                            await setUpListeners(storageUser);
                            await SplashScreen.hide();
                            return
                        }
                    }

                    await signInWithCustomToken(auth, fToken)
                        .then(async (userCredential) => {
                            // Signed in
                            const user = userCredential.user;
                            if (CheckLocal() && !isAutoAuth) {
                                await setStorageUser(user);
                            }
                            // await setUpListeners(user);
                            await SplashScreen.hide();
                        })
                        .catch(async (error) => {
                            const errorCode = error.code;
                            const errorMessage = error.message;
                            console.error(errorCode, errorMessage);
                            store.dispatch(logout());
                            await SplashScreen.hide();
                        });
                }

                newSignInSetup();
            }
        });
    } else {
        // console.log('already boostrapped');
    }
}

export const setUpListeners = async (user: any) => {
    const db = getFirestore();
    const key: string = store.getState().auth.event_key;

    await (async function () {
        // Listen for user details
        const unsubscribe = onSnapshot(doc(db, `events/${key}/users`, user.uid), (doc) => {
            const data: any = doc.data();
            const videoCallState = data.callStatus || { status: videoCallStatus.available }

            const user: User = {
                id: data.id,
                firstName: data.firstName,
                lastName: data.lastName,
                email: data.email,
                organisation: data.organisation,
                jobTitle: data.jobTitle,
                profilePicture: data.profilePicture,
                callStatus: videoCallState,
                online: data.online || false,
                isInit: true
            }

            const timestamps = data.timestamps;
            const answers: Array<UserAnswer> = data.answers;
            const matchmaking: boolean = data.matchmaking ? Boolean(data.matchmaking) : false;

            store.dispatch(setUser(user));
            store.dispatch(setUserId(user.id));
            store.dispatch(setUserTimestamps(timestamps));
            store.dispatch(setUserMatchmaking(matchmaking));
            store.dispatch(setUserAnswers(answers));
        })

        unsubscribers.push(unsubscribe);
        // console.log('subscribed to user listener')
    })();

    // Listen for user settings
    await (async function () {
        const unsubscribe = onSnapshot(doc(db, `events/${key}/users/${user.uid}/private`, 'settings'), (doc) => {
            const data: any = doc.data();
            const blockedData = (data.blocked as { [key: string]: Timestamp });
            const blockedByData = (data.blockedBy as { [key: string]: Timestamp });

            const blocked: { [key: string]: number } = {};
            if (typeof blockedData === 'object') {
                Object.keys(blockedData).forEach((key: string) => {
                    blocked[`uid-${key}`] = parseInt(String(blockedData[key].seconds));
                });
            }

            let blockedBy: { [key: string]: number } = {};
            if (typeof blockedByData === 'object') {
                Object.keys(blockedByData).forEach((key: string) => {
                    blockedBy[`uid-${key}`] = parseInt(String(blockedByData[key].seconds));
                });
            }

            const userSettings: UserSettings = {
                missedChatEmail: data.missedChatEmail,
                missedChatFrequency: data.missedChatFrequency,
                pushNotifications: data.pushNotifications,
                status: data.status,
                blocked,
                blockedBy,
                isInit: true,
            }

            store.dispatch(setUserSettings(userSettings));
            PostMessageToParent({ type: 'userStatus', message: data.status });
        })

        unsubscribers.push(unsubscribe);
        // console.log('subscribed to user settings listener')
    })();

    await (async function () {
        const ubsubscribe = onSnapshot(doc(db, `events/${key}`), async (doc) => {
            const details: any = doc.data();
            store.dispatch(updateEventToggles(details.toggles));

            const promises: Array<Promise<Unsubscribe>> = [];
            const eKey: string = key;
            const fifo: Array<string> = [];
            Object.keys(details.toggles).forEach((key: string) => {
                switch (key) {
                    case 'chat_rooms':
                        if (details.toggles[key] === true
                            && ('ROOM_LISTENER' in unsubscriberss === false || unsubscriberss['ROOM_LISTENER'] === undefined)) {
                            fifo.push('ROOM_LISTENER');
                            promises.push(roomSnapshotListener(db, eKey));
                        }

                        if ('ROOM_LISTENER' in unsubscriberss && unsubscriberss['ROOM_LISTENER'] !== undefined) {
                            unsubscriberss['ROOM_LISTENER']();
                            unsubscriberss['ROOM_LISTENER'] = undefined;

                            // console.log('unsubscribed to rooms');

                            while (roomHooks.length > 0) {
                                const room = roomHooks.shift();

                                room?.unsubscribers.details();
                                // console.log(`unsubscribed to room: ${room?.id} details listener`);

                                room?.unsubscribers.messages();
                                // console.log(`unsubscribed to room: ${room?.id} details messages`);
                            }
                            store.dispatch(resetRoomSlice());
                        }
                        break;
                    case 'direct_messaging':
                        if (details.toggles[key] === true
                            && ('DM_LISTENER' in unsubscriberss === false || unsubscriberss['DM_LISTENER'] === undefined)) {
                            fifo.push('DM_LISTENER');
                            promises.push(groupSnapshotListener(db, eKey, user));
                        }

                        if ('DM_LISTENER' in unsubscriberss && unsubscriberss['DM_LISTENER'] !== undefined) {
                            unsubscriberss['DM_LISTENER']();
                            unsubscriberss['DM_LISTENER'] = undefined;

                            // console.log('unsubscribed to groups');

                            while (groupHooks.length > 0) {
                                const group = groupHooks.shift();

                                group?.unsubscribers.details();
                                // console.log(`unsubscribed to group: ${group?.id} details listener`);

                                group?.unsubscribers.messages();
                                // console.log(`unsubscribed to group: ${group?.id} details messages`);
                            }

                            store.dispatch(resetGroupSlice());
                        }
                        break;
                    case 'filters':
                        if (details.toggles[key] === true
                            && ('FILTER_LISTENER' in unsubscriberss === false || unsubscriberss['FILTER_LISTENER'] === undefined)) {
                            fifo.push('FILTER_LISTENER');
                            promises.push(filterSnapshotListener(db, eKey));
                        }

                        if ('FILTER_LISTENER' in unsubscriberss && unsubscriberss['FILTER_LISTENER'] !== undefined) {
                            unsubscriberss['FILTER_LISTENER']();
                            unsubscriberss['FILTER_LISTENER'] = undefined;

                            // store.dispatch(updateFilters([]));
                        }
                        break;
                    case 'matchmaking':
                        if (details.toggles[key] === true
                            && ('QUESTION_LISTENER' in unsubscriberss === false || unsubscriberss['QUESTION_LISTENER'] === undefined)) {
                            fifo.push('QUESTION_LISTENER');
                            promises.push(questionSnapshotListener(db, eKey));
                            // console.log('subscribing to questions')
                        }

                        if (details.toggles[key] === true
                            && ('MATCHES_LISTENER' in unsubscriberss === false || unsubscriberss['MATCHES_LISTENER'] === undefined)) {
                            fifo.push('MATCHES_LISTENER');
                            promises.push(matchesSnapshotListener(db, eKey, user));
                            // console.log('subscribing to matches')
                        }

                        if (details.toggles[key] === false) {
                            if ('QUESTION_LISTENER' in unsubscriberss && unsubscriberss['QUESTION_LISTENER'] !== undefined) {
                                unsubscriberss['QUESTION_LISTENER']();
                                unsubscriberss['QUESTION_LISTENER'] = undefined;

                                store.dispatch(setQuestions([]));
                                // console.log('unsubscribed to questions')
                            }

                            if ('MATCHES_LISTENER' in unsubscriberss && unsubscriberss['MATCHES_LISTENER'] !== undefined) {
                                unsubscriberss['MATCHES_LISTENER']();
                                unsubscriberss['MATCHES_LISTENER'] = undefined;

                                store.dispatch(setMatches([]));
                                // console.log('unsubscribed to matches')
                            }
                        }
                        break;
                    default:
                        console.error('unknown event toggle', key);
                        break;
                }
            })

            await Promise.all(promises)
                .then((response) => {
                    const dd = response;
                    while (fifo.length > 0) {
                        const action = String(fifo.shift());
                        const unsubscriber = dd.shift();

                        unsubscriberss[action] = unsubscriber;
                    }
                })

            // console.log(unsubscriberss)
        });
    })();

    const promises = [];

    const statusPromise = (async function () {
        // Fetch the current user's ID from Firebase Authentication.
        const firebase = getDatabase();
        var uid = user.uid;

        // Create a reference to this user's specific status node.
        // This is where we will store data about being online/offline.
        var userStatusDatabaseRef = ref(firebase, `chat_data_collection/${key}/status/` + uid);

        // We'll create two constants which we will write to 
        // the Realtime database when this device is offline
        // or online.
        var isOfflineForDatabase = {
            state: 'offline',
            last_changed: Timestamp.now(),
        };

        var isOnlineForDatabase = {
            state: 'online',
            last_changed: Timestamp.now(),
        };

        // Create a reference to the special '.info/connected' path in 
        // Realtime Database. This path returns `true` when connected
        // and `false` when disconnected.
        onValue(ref(firebase, '.info/connected'), function (snapshot) {
            // If we're not currently connected, don't do anything.
            if (snapshot.val() == false) {
                // If we are offline and current status is not manually set
                // then update it to offline
                const current = store.getState().user.settings.status
                if (current.manual === false) {
                    store.dispatch(updateUserStatus({
                        status: 'offline',
                        manual: false,
                    }));
                }

                return;
            };

            // If we are currently connected, then use the 'onDisconnect()' 
            // method to add a set which will only trigger once this 
            // client has disconnected by closing the app, 
            // losing internet, or any other means.
            onDisconnect(userStatusDatabaseRef).set(isOfflineForDatabase).then(function () {
                // console.log('online')
                // The promise returned from .onDisconnect().set() will
                // resolve as soon as the server acknowledges the onDisconnect() 
                // request, NOT once we've actually disconnected:
                // https://firebase.google.com/docs/reference/js/firebase.database.OnDisconnect

                // We can now safely set ourselves as 'online' knowing that the
                // server will mark us as offline once we lose connection.
                set(userStatusDatabaseRef, isOnlineForDatabase);

                // If we are online and current status is not manually set
                // them update it to online
                const current = store.getState().user.settings.status
                if (current.manual === false) {
                    store.dispatch(updateUserStatus({
                        status: 'online',
                        manual: false,
                    }));
                }
            });
        });
    })();

    promises.push(statusPromise);

    await Promise.all(promises)
}

export const sendMessage = async (message: string, id: number | string) => {
    return new Promise(async (resolve) => {
        if (message.length > 0) {
            const filters = selectChatFilters(store.getState());
            const db = getFirestore();
            const key: string = store.getState().auth.event_key;
            const user: User = store.getState().user.user;

            if (filters.length > 0) {
                if (message.match(new RegExp(filters))) {
                    resolve('ILLEGAL');
                    return
                }
            }

            if (message.length >= 500) {
                resolve('EXCEEDED');
                return
            }

            const payload: MessagePayload = {
                id: null,
                message: message,
                sender: {
                    id: user.id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    jobTitle: user.jobTitle,
                    organisation: user.organisation,
                    profilePicture: user.profilePicture,
                    email: user.email,
                    online: user.online,
                    callStatus: user.callStatus,
                },
                status: "sent",
                recipient: id,
                systemMessage: false,
                timestamp: Timestamp.now(),
            }

            const batch = writeBatch(db);

            batch.set(
                doc(collection(db, `events/${key}/messages`)),
                payload,
            );

            batch.commit();
            resolve('SENT');
        }
    })
}

export const getDMGroup = async (recipient: number) => {
    const key: string = store.getState().auth.event_key;

    // Check if there is already a group chat between users
    const id = checkDMExists(recipient);
    if (id.length > 0) {
        return id
    }

    const functions = getFunctions(getApp(), "europe-west2");
    const addDM = httpsCallable(functions, 'createDM');

    // Create the group chat
    const data: any = await addDM({
        recipient,
        key,
    });

    if (data.data.groupId) {
        // const groupCurrentCount = store.getState().groups.count || 0;
        // store.dispatch(setGroupCount(groupCurrentCount + 1));
        setUpGroupSnaphots(data.data.groupId);
        return data.data.groupId;
    } else {
        return null;
    }
}

export const roomHooks: Array<{
    id: string,
    unsubscribers: {
        details: Unsubscribe,
        messages: Unsubscribe,
    }
}> = [];

export const groupHooks: Array<{
    id: string,
    unsubscribers: {
        details: Unsubscribe,
        users: Unsubscribe,
        messages: Unsubscribe,
    }
}> = [];

function setUpGroupSnaphots(id: string) {
    const db = getFirestore();
    const key: string = store.getState().auth.event_key;
    const existedGroup = store.getState().groups.groups.filter(group => group.id === id);
    if (existedGroup.length > 0) { return };

    const details = onSnapshot(doc(db, `events/${key}/groups`, id), (doc) => {
        const data: any = doc.data();
        var messages: Array<Message> = [];
        var users: Array<User> = [];

        const existing = store.getState().groups.groups.filter((group: Group) => group.id == data.id)
        if (existing.length > 0) {
            messages = existing[0].messages;
            users = existing[0].users;
        }

        const group: Group = {
            id: data.id,
            name: data.name,
            directMessage: data.directMessage,
            admins: data.admins,
            members: data.members,
            meta: {
                createdBy: data.meta.createdBy,
                thumbnail: data.meta.thumbnail,
            },
            modifiedAt: data.modifiedAt.toDate().toUTCString(),
            createdAt: data.createdAt.toDate().toUTCString(),
            messages,
            users,
        };

        store.dispatch(setGroup(group));
    });

    const users = onSnapshot(
        query(collection(db, `events/${key}/groups/${id}/users`)),
        (querySnapshot) => {
            const users: Record<number, User> = {};
            querySnapshot.forEach((doc) => {
                const data = doc.data();
                const user: User = {
                    id: data.id,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    email: data.email,
                    jobTitle: data.jobTitle,
                    organisation: data.organisation,
                    profilePicture: data.profilePicture,
                    online: data.online,
                    callStatus: data.callStatus
                    // timestamps: data.timestamps,
                };

                users[user.id] = user;
            });

            store.dispatch(updateGroupUsers({
                id,
                users,
            }));
        }
    );

    const messages = onSnapshot(
        query(
            collection(db, `events/${key}/messages`),
            where('recipient', '==', id),
            limitToLast(50),
            orderBy('timestamp', 'asc')
        ), (querySnapshot) => {
            const messages: Array<Message> = [];
            querySnapshot.docChanges().forEach((change) => {
                if (change.type == "added") {
                    const message = change.doc.data();

                    // Check if the user is not from a blocked user
                    if (userNotBlocked(message.sender.id, message.timestamp.seconds) && userNotBlockedBy(message.sender.id)) {
                        let sender: User = {
                            id: message.sender.id,
                            firstName: message.sender.firstName,
                            lastName: message.sender.lastName,
                            jobTitle: message.sender.jobTitle,
                            organisation: message.sender.organisation,
                            profilePicture: message.sender.profilePicture,
                            email: message.sender.email,
                            online: message.sender.online,
                            callStatus: message.sender.callStatus || { status: videoCallStatus.available },
                            // timestamps: message.sender.timestamps,
                        }

                        messages.push({
                            id: change.doc.id,
                            message: message.message,
                            sender,
                            status: message.status,
                            recipient: message.recipient,
                            systemMessage: message.systemMessage,
                            timestamp: message.timestamp.seconds
                        });

                        PostMessageToParent({ type: 'newChats' });
                    }
                } else if (change.type == "removed") {
                    // pending handling
                } else if (change.type == "modified") {
                    // pending handling
                }
            })

            if (messages.length > 0) {
                const last = messages[messages.length - 1];
                updateUserLastRead(id, last.timestamp);
                store.dispatch(updateGroupMessage({
                    id,
                    messages,
                }))
            }
        });

    groupHooks.push({
        id,
        unsubscribers: {
            details,
            users,
            messages,
        },
    });

    unsubscribers.push(details);
    unsubscribers.push(users);
    unsubscribers.push(messages);
}

// setup load more group messages function
export async function getNextPageGroupMessages(id: string, lastMessageId: string) {
    const db = getFirestore();
    const key: string = store.getState().auth.event_key;

    const olderMessages: Array<Message> = [];

    // get last message doc using id
    const lastMessageDoc = await getDoc(doc(db, `events/${key}/messages/${lastMessageId}`));

    const messageQuery = query(
        collection(db, `events/${key}/messages`),
        where('recipient', '==', id),
        limitToLast(50),
        orderBy('timestamp', 'asc'),
        endBefore(lastMessageDoc)
    );

    const querySnapshot = await getDocs(messageQuery);

    querySnapshot.forEach(doc => {
        const message = doc.data();
        // Check if the user is not from a blocked user
        if (userNotBlocked(message.sender.id, message.timestamp.seconds) && userNotBlockedBy(message.sender.id)) {
            let sender: User = {
                id: message.sender.id,
                firstName: message.sender.firstName,
                lastName: message.sender.lastName,
                jobTitle: message.sender.jobTitle,
                organisation: message.sender.organisation,
                profilePicture: message.sender.profilePicture,
                email: message.sender.email,
                online: message.sender.online,
                callStatus: message.sender.callStatus || { status: videoCallStatus.available },
                // timestamps: message.sender.timestamps,
            }

            olderMessages.push({
                id: doc.id,
                message: message.message,
                sender,
                status: message.status,
                recipient: message.recipient,
                systemMessage: message.systemMessage,
                timestamp: message.timestamp.seconds
            });
        }
    });
    return olderMessages;
    // olderMessages.length > 0 && store.dispatch(addOlderGroupMessagesBulk({
    //     id: id,
    //     messages: olderMessages
    // }));

}


function setUpRoomSnapshots(id: string) {
    const db = getFirestore();
    const key: string = store.getState().auth.event_key;

    // First set up listener for room itself
    const details = onSnapshot(doc(db, `events/${key}/rooms`, id), (doc) => {
        const data: any = doc.data();

        store.dispatch(addRoom({
            id: parseInt(doc.id),
            name: data.name,
            order: data.order,
        }));
    });

    // Then set up listener for room messages
    const messages = onSnapshot(
        query(
            collection(db, `events/${key}/messages`),
            where('recipient', '==', parseInt(id)),
            limitToLast(50),
            orderBy('timestamp', 'asc'))
        , (querySnapshot) => {
            const added: Array<Message> = [];
            querySnapshot.docChanges().forEach((change) => {
                if (change.type == "added") {
                    const message = change.doc.data();

                    // Check if the user is not from a blocked user
                    if (userNotBlocked(message.sender.id, message.timestamp.seconds) && userNotBlockedBy(message.sender.id)) {
                        let sender: User = {
                            id: message.sender.id,
                            firstName: message.sender.firstName,
                            lastName: message.sender.lastName,
                            jobTitle: message.sender.jobTitle,
                            organisation: message.sender.organisation,
                            profilePicture: message.sender.profilePicture,
                            email: message.sender.email,
                            online: message.sender.online,
                            callStatus: message.sender.callStatus || { status: videoCallStatus.available },
                            // timestamps: message.sender.timestamps,
                        }

                        added.push({
                            id: change.doc.id,
                            message: message.message,
                            sender,
                            timestamp: message.timestamp.seconds,
                            recipient: message.recipient,
                            systemMessage: message.systemMessage,
                            status: message.status
                        });

                        PostMessageToParent({ type: 'newChats' });
                    }
                } else if (change.type == "removed") {
                    store.dispatch(deleteMessage({
                        id: parseInt(id),
                        message: change.doc.id,
                    }));
                } else if (change.type == "modified") {
                }
            });

            if (added.length > 0) {
                const last = added[added.length - 1];
                updateUserLastRead(parseInt(id), last.timestamp);

                store.dispatch(addMessageBulk({
                    id: parseInt(id),
                    added,
                }));
            }
        });
    roomHooks.push({
        id,
        unsubscribers: {
            details,
            messages,
        }
    });

    unsubscribers.push(details);
    unsubscribers.push(messages);
}

// setup load more room message function
export async function getNextPageRoomMessages(id: string, lastMessageId: string) {

    const db = getFirestore();
    const key: string = store.getState().auth.event_key;

    const olderMessages: Array<Message> = [];

    // get last message doc using id
    const lastMessageDoc = await getDoc(doc(db, `events/${key}/messages/${lastMessageId}`));

    //return
    const messageQuery = query(
        collection(db, `events/${key}/messages`),
        where('recipient', '==', parseInt(id)),
        limitToLast(50),
        orderBy('timestamp', 'asc'),
        endBefore(lastMessageDoc)
    );
    const querySnapshot = await getDocs(messageQuery);
    querySnapshot.forEach(doc => {
        const message = doc.data();
        if (userNotBlocked(message.sender.id, message.timestamp.seconds && userNotBlockedBy(message.sender.id))) {
            let sender: User = {
                id: message.sender.id,
                firstName: message.sender.firstName,
                lastName: message.sender.lastName,
                jobTitle: message.sender.jobTitle,
                organisation: message.sender.organisation,
                profilePicture: message.sender.profilePicture,
                email: message.sender.email,
                online: message.sender.online,
                callStatus: message.sender.callStatus || { status: videoCallStatus.available },
                // timestamps: message.sender.timestamps,
            }

            olderMessages.push({
                id: doc.id,
                message: message.message,
                sender,
                timestamp: message.timestamp.seconds,
                recipient: message.recipient,
                systemMessage: message.systemMessage,
                status: message.status
            });
        }
    });

    return olderMessages;
    // olderMessages.length > 0 && store.dispatch(addOlderMessageBulk({
    //     id: parseInt(id),
    //     added: olderMessages
    // }));

}

export function userNotBlocked(uid: string, time?: number): boolean {
    const ident = `uid-${uid}`;

    if (ident in store.getState().user.settings.blocked) {
        if (time === undefined) {
            return false;
        }

        const seconds = store.getState().user.settings.blocked[ident];
        return seconds > time;
    }

    return true;
}

export function userNotBlockedBy(uid: string, time?: number): boolean {
    const ident = `uid-${uid}`;

    if (ident in store.getState().user.settings.blockedBy) {
        if (time === undefined) {
            return false;
        }

        const seconds = store.getState().user.settings.blockedBy[ident];
        return seconds > time;
    }

    return true;
}

export async function videoCallGroup(groupId: string): Promise<number[]> {
    const key: string = store.getState().auth.event_key;
    const functions = getFunctions(getApp(), "europe-west2");
    const startCall = httpsCallable(functions, 'startVideoCallWithUser');

    // start the video call for the group
    const data: any = await startCall({
        key, groupId
    });

    return data.data;
}

export async function pickUpVideoCall(groupId: string): Promise<void> {
    const userId = store.getState().user.user.id;
    const key: string = store.getState().auth.event_key;

    const userRef = doc(
        getFirestore(),
        `events/${key}/users/${userId}`
    );

    await updateDoc(userRef, {
        callStatus: { groupId, status: videoCallStatus.answeredCall },
    });
}

export async function hangUpVideoCall(groupId: string): Promise<number[]> {
    try {
        const userId = store.getState().user.user.id;
        const key: string = store.getState().auth.event_key;

        const userRef = doc(
            getFirestore(),
            `events/${key}/users/${userId}`
        );

        // update current user immediately to reflect in UI
        await updateDoc(userRef, {
            callStatus: { status: videoCallStatus.available }
        });

        const functions = getFunctions(getApp(), "europe-west2");

        // hangup call for other users using function
        const hangUpCall = httpsCallable(functions, 'endVideoCallForGroup');

        // hang up the video call for the group
        const data: any = await hangUpCall({
            key, groupId, endpoint: process.env.REACT_APP_WEBCAM_API_URL || 'https://api.streamgo.vc'
        });

        return data.data;
    } catch (err) {
        console.error(err)
        return []
    }
}

export const videoCallRecipient = (recipientId: number, setLoadingVideoCall: (loading: boolean) => void, showAlert: (alert: string) => void) => {
    let groupId: string

    setLoadingVideoCall(true);
    getDMGroup(recipientId)
        .then((id: string) => {
            groupId = id;
            return videoCallGroup(id);
        })
        .catch((err) => {
            console.error(err);
            if (err.toString().startsWith('FirebaseError')) {
                showAlert(err.toString().substring(15));
            } else {
                showAlert('Could not start the call');
            }

            if (groupId)
                hangUpVideoCall(groupId);
            setLoadingVideoCall(false);
        });
}

export async function getDolbyToken(groupId: string): Promise<DolbyLoginResponse | null> {
    try {
        const key: string = store.getState().auth.event_key;
        const functions = getFunctions(getApp(), "europe-west2");

        const getDolbyToken = httpsCallable(functions, 'getDolbyToken');

        // get the Dolby Token needed for connecting
        const data: any = await getDolbyToken({
            key, groupId, endpoint: process.env.REACT_APP_WEBCAM_API_URL || 'https://api.streamgo.vc'
        });

        return data.data;
    } catch (err) {
        console.error(err)
        return null
    }
}

export const listenForUser = (id: number, callback: (data: any) => void): Unsubscribe => {
    const db = getFirestore();
    const key: string = store.getState().auth.event_key;

    const unsubscribe = onSnapshot(doc(db, `events/${key}/users`, String(id)), (doc) => {
        const data: any = doc.data();
        callback(data);
    })

    return unsubscribe;
}

export const getUserById = async (id: number) => {
    const db = getFirestore();
    const key: string = store.getState().auth.event_key;
    const userRef = doc(db, `events/${key}/users/${id}`)
    const snap = await getDoc(userRef);
    if (snap.exists()) {
        return snap.data()
    }
}

export async function fetchBlockedUserDetails() {
    const list = Object.keys(store.getState().user.settings.blocked);
    const users: Array<User> = [];

    if (list.length <= 0) { return users };

    const db = getFirestore();
    const key: string = store.getState().auth.event_key;

    for (const uid of list) {
        const id = parseInt(uid.replace("uid-", ""));
        const blockUserRef = doc(db, `events/${key}/users/${id}`)
        const blockUserSnap = await getDoc(blockUserRef);
        if (blockUserSnap.exists()) {
            users.push(blockUserSnap.data() as User);
        }
    }

    // const chunkArray = (list: any[], chunk: number): any[][] => {
    //     const result = [];

    //     for (let i = 0; i < list.length; i += chunk) {
    //         result.push(list.slice(i, i + chunk));
    //     }

    //     return result;
    // };

    // const chunks = chunkArray(list, 1);
    // console.log(chunks);

    // for await (const snap of chunks.map(
    //     async (chunk) => {
    //         const processed = chunk.map((chunk) => parseInt(chunk.replace("uid-", "")));
    //         console.log(`processed`, processed);
    //         return await getDocs(
    //             query(
    //                 collection(db, `events/${key}/users`),
    //                 where('id', 'in', processed)
    //             )
    //         )
    //     }
    // )) {
    //     console.log(snap);
    //     snap.forEach((doc) => {
    //         console.log(doc.data());
    //     users.push((doc.data()) as User);
    //   })
    // }

    return users;
}

export function updateUserLastRead(id: number | string, timestamp: number) {
    const currentChannel = store.getState().user.currentChannel;
    const lastTime = store.getState().user.timestamps[id];
    const key: string = store.getState().auth.event_key;
    if (currentChannel.includes(id) && (lastTime === undefined || timestamp > parseInt(lastTime))) {
        // update users last read
        const userRef = doc(
            getFirestore(),
            `events/${key}/users/${store.getState().user.user.id}`,
        );

        updateDoc(userRef, {
            [`timestamps.${id}`]: timestamp,
        });
    }
}

export function initUserTimestamp(id: number | string, timestamp: number) {
    // const currentChannel = store.getState().user.currentChannel;
    const lastTime = store.getState().user.timestamps[id];
    const key: string = store.getState().auth.event_key;
    if (lastTime === undefined || timestamp > parseInt(lastTime)) {
        // update users last read
        const userRef = doc(
            getFirestore(),
            `events/${key}/users/${store.getState().user.user.id}`,
        );

        updateDoc(userRef, {
            [`timestamps.${id}`]: timestamp,
        });
    }
}

export async function userLogout() {
    if (getApps().length === 0) {
        // console.log('app not initialised yet, no logout will be called.');
        return
    }
    const userId = store.getState().user.user.id;
    const key: string = store.getState().auth.event_key;

    const userRef = doc(
        getFirestore(),
        `events/${key}/users/${userId}`
    );

    await updateDoc(userRef, {
        online: false,
    });

    if (await getStorageUser()) {
        removeStorageUser();
    }

    const auth = getAuth();
    signOut(auth).then(() => {
        store.dispatch(logout());
        window.location.href = `/`;
    })
        .catch((error) => {
            console.error(error)
        });
}

const questionSnapshotListener = async (db: Firestore, key: string) => {
    const q = query(collection(db, `events/${key}/questions`));

    const unsubscribe = onSnapshot(q, (querySnapshot) => {
        const questions: Array<Question> = [];
        querySnapshot.forEach((doc) => {
            let data = doc.data();
            const question: Question = {
                id: parseInt(doc.id),
                question: data.question,
                type: data.type,
                answers: data.answers,
            }

            questions.push(question)
        })

        store.dispatch(setQuestions(questions));
    });

    return unsubscribe;
}

const matchesSnapshotListener = async (db: Firestore, key: string, user: any) => {
    const matches = collection(db, `events/${key}/matches`);
    const unsubscribe = onSnapshot(query(matches, orderBy('match', 'desc'), limit(360), where('subject', '==', parseInt(user.uid))), (querySnapshot) => {
        const matches: Array<Match> = [];
        querySnapshot.forEach((doc) => {
            let data = doc.data();

            matches.push({
                match: data.match,
                user: data.user,
            });
        })

        store.dispatch(setMatches(matches))
    });

    return unsubscribe;
}

const filterSnapshotListener = async (db: Firestore, key: string) => {
    const filters = query(
        collection(db, `events/${key}/filters`)
    );

    const unsubscribe = onSnapshot(filters, (querySnapshot) => {
        const filters: Array<EventFilter> = [];
        querySnapshot.forEach((change) => {
            const filter = change.data();
            filters.push(filter as EventFilter);
        })

        let chatExpress = '';
        var allowUser = true;
        const user = selectUser(store.getState());
        filters.forEach(filter => {
            // escape characters before regex i.e. things like . and \ etc
            const value = filter.value.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            // for chat rules, only pay attention to ones that dissallow
            if (filter.type === 'chat' && filter.allow === false) {
                if (filter.wildcard === true) {
                    chatExpress += `${filter.value}|`;
                } else {
                    chatExpress += `\\b${filter.value}\\b|`;
                }
            } else if (filter.type === 'email') {
                if (filter.wildcard === true) {
                    var expression = `/(${value})/`;
                } else {
                    var expression = `/(\\b${value}\\b)/`;
                }

                // if (user.email.match(new RegExp(expression)) && filter.allow === false) {
                if (user.email === filter.value && !filter.allow) {
                    userLogout();
                }
            }
        });

        if (chatExpress.length > 0) {
            chatExpress = chatExpress.substring(0, chatExpress.length - 1);
            const regex = new RegExp(`(${chatExpress})`);

            store.dispatch(updateFilters(regex.source))
        }
    })

    return unsubscribe;
}

const roomSnapshotListener = async (db: Firestore, key: string) => {
    const rooms = query(collection(db, `events/${key}/rooms`));

    const roomsData: { [key: number]: Room } = {};
    const querySnapshot = await getDocs(rooms);
    querySnapshot.forEach((doc) => {
        const data: any = doc.data();

        roomsData[parseInt(doc.id)] = {
            id: parseInt(doc.id),
            name: data.name,
            order: data.order,
        };
    });

    store.dispatch(setRoomCount(Object.keys(roomsData).length));

    store.dispatch(addRoomBulk(roomsData));

    Object.keys(roomsData).forEach(id => {
        setUpRoomSnapshots(id);
    });

    // Set up mass listener
    // this listens for creation/ deletion events for rooms
    const unsubscribe = onSnapshot(rooms, (querySnapshot) => {
        // const source = doc.metadata.hasPendingWrites ? "Local" : "Server";
        if (store.getState().rooms.count !== null && querySnapshot.size !== store.getState().rooms.count && (store.getState().rooms.count ?? 0) < Object.keys(roomsData).length) {
            querySnapshot.docChanges().forEach((change) => {
                if (change.type == "added") {
                    store.dispatch(incrementCount());
                    setUpRoomSnapshots(change.doc.id);
                }

                if (change.type == "removed") {
                    const hooks = roomHooks.filter(hook => hook.id === change.doc.id);

                    if (hooks.length === 1) {
                        hooks[0].unsubscribers.details();
                        hooks[0].unsubscribers.messages();

                        store.dispatch(deleteRoom(parseInt(change.doc.id)));
                    } else {
                        console.error('cannot find unsubscibe methods for ' + change.doc.id);
                    }
                }
            })
        }
    });

    return unsubscribe;
}

const groupSnapshotListener = async (db: Firestore, key: string, user: any) => {
    // Get individual ids of groups
    // so we can add individual listeners
    const groups = query(
        collection(db, `events/${key}/groups`),
        where("members", "array-contains", parseInt(user.uid)),
        orderBy('meta.lastSent', 'desc'));

    let ids: Array<string> = [];
    const querySnapshot = await getDocs(groups);

    querySnapshot.forEach((doc) => {
        ids.push(doc.id);
    });

    store.dispatch(setGroupCount(ids.length));

    ids.forEach(id => {
        setUpGroupSnaphots(id);
    });

    // Set up mass listener
    // this listens for creation/ deletion events
    const unsubscribe = onSnapshot(groups, (querySnapshot) => {
        if (querySnapshot.size !== store.getState().groups.count) {
            // console.log('found change?', querySnapshot.size);
            querySnapshot.docChanges().forEach((change) => {
                if (change.type == "added") {
                    setUpGroupSnaphots(change.doc.id);
                    const groupCurrentCount = store.getState().groups.count || 0;
                    store.dispatch(setGroupCount(groupCurrentCount + 1));
                }

                if (change.type == "removed") {
                    const hooks = groupHooks.filter(hook => hook.id === change.doc.id);

                    if (hooks.length === 0) {
                        hooks[0].unsubscribers.details();
                        hooks[0].unsubscribers.messages();
                        hooks[0].unsubscribers.users();
                    } else {
                        console.error('cannot find unsubscibe methods for ' + change.doc.id);
                    }
                }
            })
        }
    })

    return unsubscribe;
}