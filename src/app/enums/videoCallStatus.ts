enum videoCallStatus {
    available = 0,
    outgoingCall = 1,
    incomingCall = 2,
    answeredCall = 3
}

export default videoCallStatus