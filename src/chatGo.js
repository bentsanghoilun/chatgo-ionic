const init = async (eventKey, settings) => {

    let finalSettings = {
        mode: settings.mode ? settings.mode : 'widget',
        embedId: settings.embedId ? settings.embedId : 'chatGoContainer',
        isAutoAuth: settings.isAutoAuth ? settings.isAutoAuth : false,
        openOnloaded: settings.openOnloaded ? settings.openOnloaded : false,
        hidden: settings.hidden ? settings.hidden : false,
        dmId: settings.dmId ? settings.dmId : null,
        desktopEnabled: settings.hasOwnProperty('desktopEnabled') ? settings.desktopEnabled : false,
        widgetPosition: settings.widgetPosition ? settings.widgetPosition : 'end bottom',
    }

    // query the scriptTag and base
    var scriptTag = document.querySelector('script[src*="chatGo.js"]');
    var base = scriptTag.getAttribute('src').replace('chatGo.js', '');
    var api = process.env.REACT_APP_CHATGO_API;

    // check event key exist
    if (!eventKey) {
        console.error('Cannot initialise Chatgo. No event key provided.');
        return;
    }

    // include css
    var cssFile = document.createElement('link');
    cssFile.id = 'ChatGoCss';
    cssFile.rel = 'stylesheet';
    cssFile.type = 'text/css';
    cssFile.href = `${base}ChatGo.css`;
    document.head.appendChild(cssFile);

    // create chatGo iFrame
    var iframe = document.createElement('IFRAME');

    // create badge
    var badge = null;
    if (finalSettings.mode === 'widget') {
        badge = document.createElement('div');
        badge.classList.add('chatgo-badge');
        finalSettings.hidden && badge.classList.add('hidden-important');
        badge.id = 'chatgo-badge';

        // create badge logo
        var badgeImg = document.createElement('img');
        badgeImg.alt = 'Chat';
        badgeImg.src = `${base}assets/chatIconWhite.png`;
        badge.appendChild(badgeImg);

        // create status dot
        var badgeStatus = document.createElement('div');
        badgeStatus.id = 'chatgo-badge-status';
        badgeStatus.classList.add('chatgo-badge-status');
        badge.appendChild(badgeStatus);

        // create new message badge
        var badgeNew = document.createElement('div');
        badgeNew.id = 'chatgo-badge-new';
        badgeNew.classList.add('chatgo-badge-new');
        badgeNew.innerHTML = 'new';
        badge.appendChild(badgeNew);
    }

    // get brandings data
    var brandingApiOptions = {
        method: 'GET',
        headers: {
            'accept': 'application/json',
            'authorization': `Bearer ${eventKey}`
        }
    }
    const brandRes = await fetch(`${api}/branding`, brandingApiOptions);
    const brandingData = await brandRes.json();
    var branding = brandingData.meta.custom.widget;
    var badgeImgSrc = branding.logo;

    if (badge && badgeImg) {
        badge.style.background = branding.colour_1;
    }
    if (badge && badgeImg && badgeImgSrc) {
        badgeImg.src = badgeImgSrc;
    }
    if (badge) { badge.style.opacity = 1 };

    var themeData = {
        event_name: brandingData.name,
        themeColors: {
            colour_1: branding.colour_1,
            colour_2: branding.colour_2
        }
    }
    await createComponets(themeData);

    function createComponets(themeData) {
        return new Promise(resolve => {
            // console.log('createComponets called');
            iframe.id = 'chatgo-iframe';
            iframe.classList.add('chatgo-iframe');
            iframe.src = `${base}index.html?eventKey=${eventKey}&isAutoAuth=${finalSettings.isAutoAuth?'1':'0'}&mode=${finalSettings.mode}`;
            iframe.onload = async () => {
                var temp = {
                    eventKey: eventKey,
                    dmId: finalSettings.dmId,
                    isAutoAuth: finalSettings.isAutoAuth,
                    event_name: themeData.event_name,
                    themeColors: themeData.themeColors,
                    source: 'init',
                    mode: finalSettings.mode
                }
                var loadedIFrame = document.getElementById('chatgo-iframe').contentWindow;
                await loadedIFrame.postMessage(temp, '*');

                //start listening to iframe messags
                messageHandler(base, finalSettings);

                resolve(true);
            }
    
            // create and append iFrame
            if (finalSettings.mode === 'widget') {

                // add float class to the iframe
                iframe.classList.add('float');
                (finalSettings.hidden) && iframe.classList.add('hidden-important');
                if (finalSettings.openOnloaded) { iframe.classList.add('open') };

                // add position class to the iframe and badge
                var xPos = finalSettings.widgetPosition.includes('start') ? 'start' : 'end';
                var yPos = finalSettings.widgetPosition.includes('top') ? 'top' : 'bottom';
                iframe.classList.add(xPos);
                iframe.classList.add(yPos);
                badge.classList.add(xPos);
                badge.classList.add(yPos);
    
                // provide badge toggle, if is widget mode
                badge.addEventListener('click', () => {
                    iframe.classList.contains('open') ? close() : open()
                });
                
                document.body.appendChild(badge);
                document.body.appendChild(iframe);
            } else if (finalSettings.mode === 'embed') {
                // check if desktop enabled?
                finalSettings.desktopEnabled && iframe.classList.add('desktop-enabled');
    
                if (settings.embedId) {
                    // query the specified element
                    var container = document.querySelector(`#${finalSettings.embedId}`);
                    if (container) {
                        container.appendChild(iframe);
                    } else {
                        console.error('No element found on the page with the specified ID');
                    }
                } else {
                    iframe.classList.add('self-contain');
                    scriptTag.insertAdjacentElement('afterend', iframe);
                }
            }
        });
    }
    return Promise.resolve(true);
}

async function authenticate(token, firebaseToken){
    var temp = {
        userToken: token,
        firebaseToken: firebaseToken,
        source: 'authenticate'
    }
    var loadedIFrame = document.getElementById('chatgo-iframe').contentWindow;
    await loadedIFrame.postMessage(temp, '*');
    return Promise.resolve(true);
}

function show() {
    var iframe = document.getElementById('chatgo-iframe');
    var badge = document.getElementById('chatgo-badge');
    var badgeNew = document.getElementById('chatgo-badge-new');
    if (badge && badge.classList.contains('hidden-important')) {
        badge.classList.remove('hidden-important');
    }
    if (iframe && iframe.classList.contains('hidden-important')) {
        iframe.classList.remove('hidden-important');
    }
    if (badgeNew) {
        badgeNew.style.display = 'none';   
    }
}
function hide(){
    var iframe = document.getElementById('chatgo-iframe');
    var badge = document.getElementById('chatgo-badge');
    if (badge && !badge.classList.contains('hidden-important')) {
        badge.classList.add('hidden-important');
    }
    if (iframe && !iframe.classList.contains('hidden-important')) {
        iframe.classList.add('hidden-important');
    }
}

function open(){
    var iframe = document.getElementById('chatgo-iframe');
    var badgeNew = document.getElementById('chatgo-badge-new');
    if (iframe) {
        iframe.classList.add('open');
    }
    if (badgeNew) {
        badgeNew.style.display = 'none';   
    }
}
function close(){
    var iframe = document.getElementById('chatgo-iframe');
    if (iframe) {
        iframe.classList.remove('open');
    }
}

function messageHandler(base, settings) {
    window.addEventListener('message', event => {
        var iframe = document.getElementById('chatgo-iframe');

        // console.log(`Got message from ${event.origin}`, `type: ${event.data.type}`, event.data);
        const type = event.data.type, message = event.data.message;
        if (type === 'close') {
            hide();
        }
        if (type === 'userStatus' && settings.mode === 'widget') {
            var badgeStatus = document.getElementById('chatgo-badge-status');
            if (badgeStatus) {
                var status = message.status;
                var color = status === 'online' ? `#28ba62` : status === 'offline' ? '#ed576b' : `#e0ac08`;
                badgeStatus.style.backgroundColor = color;
            }
        }
        if (type === 'newChats' && settings.mode === 'widget' && !iframe.classList.contains('open')) {
            var badgeNew = document.getElementById('chatgo-badge-new');
            badgeNew.style.display = 'inline-block';
        }
    });
}

export { init, authenticate, show, hide, open, close };