import { IonContent, IonHeader, IonIcon, IonList, IonSearchbar, IonText, IonToolbar, isPlatform, getPlatforms, IonPage } from "@ionic/react";
import { alertCircleOutline, searchOutline } from "ionicons/icons";
import { Fragment, useEffect, useState } from "react";
import { useAppSelector } from '../../app/hooks';
import { useUserSearchQuery } from "../../app/services/userApi";
import { selectUser } from '../../app/slices/userSlice';
import UserListSkeleton from "../../components/Skeleton/UserListSkeleton";
import TabHeader from "../../components/TabHeader";
import UserItem from "../../components/UserItem";
import { User } from '../Auth/types';

const UserList: React.FC = props => {
    const [searchText, setSearchText] = useState('');
    const { data = [], isFetching } = useUserSearchQuery(searchText, {
        skip: searchText.length === 0,
    });
    
    const user: User = useAppSelector(selectUser);
    const [result, setResult] = useState(data);

    useEffect(() => {
        // console.log('isFetching', isFetching);
        // console.log('data', data);
        if (isFetching) {
            data.length > 0 && setResult([]);
        } else {
            searchText!== '' && setResult(data);
        }
    }, [isFetching, data, searchText]);

    return(
        <IonPage>
            <TabHeader title='Connect With Attendees'>
                <IonToolbar>
                    <IonSearchbar
                        animated
                        value={searchText}
                        // onKeyUp={e => changeSearchText(e)}
                        onIonChange={e => setSearchText(e.detail.value!)}
                        debounce={600}
                        className={isPlatform('ios') ? 'ion-margin-top' : 'ion-no-margin'}
                        placeholder={`eg. Sales Manager`}
                    ></IonSearchbar>
                </IonToolbar>     
            </TabHeader>
            <IonContent>
                {
                    searchText === '' ?
                        <div className='ion-padding ion-margin-top ion-text-center ion-align-items-center' style={{
                            display: 'flex',
                            flexDirection: 'column',
                            maxWidth: '420px',
                            margin: 'auto'
                        }}>
                            <IonIcon icon={searchOutline} size='large' />
                            <IonText><p>Try searching by job title or company name</p></IonText>
                        </div> :
                        isFetching ? <UserListSkeleton count={6} />
                            : result.length > 0 ?
                                    <IonList lines='none'>
                                    {
                                        result.map((item: any, index: any) => (
                                            item.id !== user.id &&
                                            <UserItem
                                                key={index}
                                                id={item.id}
                                                index={index}
                                                firstName={item.first_name}
                                                lastName={item.last_name}
                                                jobTitle={item.job_title}
                                                organisation={item.organisation}
                                                status={"disabled"}
                                                profileImage={item.profile_picture}
                                            />
                                        ))
                                    }
                                    </IonList>
                                    :
                                    <div className='ion-padding ion-margin-top ion-text-center ion-align-items-center' style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                        maxWidth: '420px',
                                        margin: 'auto'
                                    }}>
                                        <IonIcon icon={alertCircleOutline} size='large' color='danger' />
                                        <IonText color='danger'><p>No attendees found. Try searching for other job titles and companies, or broadening your search.</p></IonText>
                                    </div>
                }
            </IonContent>
        </IonPage>
    )
}

export default UserList;