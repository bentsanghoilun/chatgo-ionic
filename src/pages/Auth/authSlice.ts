import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";
import { UserState } from "./types";
import CheckLocal from "../../components/Classes/CheckLocal";

const isWeb = window.self !== window.top;
// get search query from url to get init autoAuth params
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const initEventKey = urlParams.get('eventKey');
const initIsAutoAuth = urlParams.get('isAutoAuth') && parseInt(String(urlParams.get('isAutoAuth'))) === 1 ? true : false;
var localAccess = CheckLocal();

const initialState: UserState = {
  token: initIsAutoAuth || !localAccess ? '' : localStorage.getItem('token') || '',
  firebase_token: initIsAutoAuth || !localAccess ? '' : localStorage.getItem('firebase_token') || '',
  event_key: isWeb ? String(initEventKey) : String(process.env.REACT_APP_EVENT_TOKEN) || '',
  status: 'idle',
}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
      setStatus: (state, action: PayloadAction<'idle' | 'loading' | 'failed'>) => {
        state.status = action.payload;
      },
      setToken: (state, { payload: { token } }: PayloadAction<{ token: string }>) => {
        state.token = token;
      },
      setEvent_key: (state, action: PayloadAction<string>) => {
        state.event_key = action.payload;
      },
      setCredentials: (state, { payload: { access_token, firebase_token } }: PayloadAction<{ access_token: string, firebase_token: string }>) => {
        state.token = access_token;
        state.firebase_token = firebase_token;
      },
      logout: (state) => {
        state.token = '';
        state.firebase_token = '';
        if (initIsAutoAuth || !localAccess) { return };
        window.self.localStorage.removeItem('token');
        window.self.localStorage.removeItem('firebase_token');
      },
    },
});

export const selectStatus = (state: RootState) => state.auth.status;
export const isAuthenticated = (state: RootState) => state.auth.token.length > 0 && state.auth.firebase_token.length > 0;
export const selectFirebaseToken = (state: RootState) => state.auth.firebase_token;
export const selectEvent_key = (state: RootState) => state.auth.event_key;

export const { setStatus, setToken, setEvent_key, setCredentials, logout } = authSlice.actions;

export default authSlice.reducer;
