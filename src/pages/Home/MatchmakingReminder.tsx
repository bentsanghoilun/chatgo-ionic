import { IonContent, IonAvatar, IonIcon, IonButton, IonPage } from "@ionic/react";
import { peopleOutline } from "ionicons/icons";
import { useEffect } from "react";
import { useHistory } from "react-router";
import { useAppSelector } from '../../app/hooks';
import { selectEventName } from '../../app/slices/initDataSlice';

interface Props {
    setCheckedMatchmaking: (bool: boolean) => void;
}

const MatchmakingReminder: React.FC<Props> = (props: Props) => {
    useEffect(() => {
        props.setCheckedMatchmaking(true);
    }, []);
    const history = useHistory();
    const event_name: string | null | undefined = useAppSelector(selectEventName);

    return (
        <IonPage>
            <IonContent>
                <div className='ion-justify-content-center ion-align-items-center' style={{
                    width: '100%',
                    height: '100%',
                    display: 'flex',
                    flexDirection: 'column',
                    boxSizing: 'border-box',
                    padding: '16px',
                    textAlign: 'center'
                }} >
                        <IonAvatar
                        className='ion-justify-content-center ion-align-items-center'
                        style={{
                            width: '76px',
                            height: '76px',
                            display: 'flex',
                            backgroundColor: 'var(--ion-color-primary)'
                        }}
                    >
                        <IonIcon size="large" color='light' icon={peopleOutline}/>
                    </IonAvatar>
                    <h2>Welcome to the {event_name ? event_name : 'ChatGo'} app</h2>
                    <p>Don't forget to complete your profile so we can connect you with other attendees</p>
                    <div className='ion-padding'>
                        <IonButton
                            color='primary'
                            onClick={() => history.push('/matchmakingsettings')}
                        >Let's do it</IonButton>
                        <IonButton
                            color='medium'
                            fill='outline'
                            onClick={() => history.push('/home')}
                        >Maybe later</IonButton>
                    </div>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default MatchmakingReminder;