import {
  IonBadge,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs, IonFooter, IonToolbar, IonTitle, IonTab, IonPage
} from '@ionic/react';
import { chatbubblesOutline, fileTrayFullOutline, searchOutline, starOutline } from 'ionicons/icons';
import { Fragment, useEffect, useState } from 'react';
import { Redirect, Route } from 'react-router';
import { useAppSelector } from '../../app/hooks';
import { selectEventToggles } from '../../app/slices/eventSlice';
import { selectGroups, selectGroupsLastMessages } from '../../app/slices/groupSlice';
import { selectMatchesCount } from '../../app/slices/matchSlice';
import { selectAllRooms, selectLastMessages } from '../../app/slices/roomSlice';
import { selectUserId, selectUserTimestamps } from '../../app/slices/userSlice';
import { EventToggles } from '../../app/types/eventToggles';
import { InitData } from '../../app/types/InitData';
import ChatRooms from '../ChatRooms/ChatRooms';
import Matches from '../Matches/Matches';
import Inbox from '../Messages/Inbox';
import UserList from '../Users/UserList';
import { initUserTimestamp, userNotBlockedBy } from '../../app/firebase';
import './Home.css';

interface Props{
  viewportInfo: { width: number, height: number };
  initData: InitData;
}

const Home: React.FC<Props> = (props: Props) => {
  const matchCount = useAppSelector(selectMatchesCount);
  const rooms = useAppSelector(selectAllRooms);
  const groups = useAppSelector(selectGroups);
  const userId = useAppSelector(selectUserId);
  const lastRoomMessages = useAppSelector(selectLastMessages);
  const lastGroupMessages = useAppSelector(selectGroupsLastMessages);
  const userTimestamps = useAppSelector(selectUserTimestamps);
  const [ roomUnreadCount, updateRoomUnreadCount ] = useState(0);
  const [groupUnreadCount, updateGroupUnreadCount] = useState(0);
  const eventToggles: EventToggles | never[] = useAppSelector(selectEventToggles);
  const [homeRoute, setHomeRoute] = useState('/chatrooms');
  

  useEffect(() => {
    var count = 0;
    rooms.forEach(room => {
      if (lastRoomMessages[room.id] && userTimestamps[room.id] && lastRoomMessages[room.id].time > parseInt(userTimestamps[room.id])
      || lastRoomMessages[room.id] && !userTimestamps[room.id]) {
        count++;
        if (lastRoomMessages[room.id] && !userTimestamps[room.id]) {
          initUserTimestamp(room.id, Math.round(new Date().getTime() / 1000))
        }
      }
    })

    updateRoomUnreadCount(count);
  }, [rooms, lastRoomMessages, userTimestamps]);

  useEffect(() => {
    var count = 0;
    groups.forEach(group => {
      if (group && group.directMessage && userId) {
        const target = group.members.find(member => member != userId);
        if (!userNotBlockedBy(String(target))) {
          return
        }
      }
      if (lastGroupMessages[group.id] && userTimestamps[group.id] && lastGroupMessages[group.id].time > parseInt(userTimestamps[group.id])
      || lastGroupMessages[group.id] && !userTimestamps[group.id]) {
        count++;
        if (lastGroupMessages[group.id] && !userTimestamps[group.id]) {
          initUserTimestamp(group.id, Math.round(new Date().getTime() / 1000))
        }
      }
    })

    updateGroupUnreadCount(count);
  }, [groups, lastGroupMessages, userTimestamps, userId]);

  useEffect(() => {
    var tempHomeRoute = homeRoute;
    if (Object.keys(eventToggles).length === 0) {
      return
    }
    if (!eventToggles.chat_rooms) {
      tempHomeRoute = '/inbox';
    }
    if (!eventToggles.direct_messaging) {
      tempHomeRoute = '/matches';
    }
    if (!eventToggles.matchmaking) {
      tempHomeRoute = '/users';
    }
    setHomeRoute(tempHomeRoute)
  }, [eventToggles]);

  return (
    <Fragment>
      <IonPage id='home'>
      <IonTabs>
        <IonRouterOutlet>
            <Route path={`/home/:tab(chatrooms)`} exact={true}>
              <ChatRooms viewportInfo={props.viewportInfo} />
            </Route>
            <Route path={`/home/:tab(matches)`} exact={true}>
              <Matches />
            </Route>
            <Route path={`/home/:tab(inbox)`} exact={true}>
              <Inbox viewportInfo={props.viewportInfo} />
            </Route>
            <Route path={`/home/:tab(users)`} exact={true}>
              <UserList />
            </Route>
          <Redirect exact path={`/home`} to={`/home${homeRoute}`} />
        </IonRouterOutlet>
        
        <IonTabBar slot="bottom">
          {
            eventToggles.chat_rooms &&
            <IonTabButton tab="chatrooms" href={`/home/chatrooms`}>
              <IonIcon icon={chatbubblesOutline} />
              <IonLabel>Chat Rooms</IonLabel>
              {
                roomUnreadCount > 0 &&
                <IonBadge>{roomUnreadCount < 99 ? roomUnreadCount : `99+`}</IonBadge>
              }
            </IonTabButton>
          }

          {
            eventToggles.matchmaking &&
            <IonTabButton tab="matches" href={`/home/matches`}>
              <IonIcon icon={starOutline} />
              <IonLabel>Matches</IonLabel>
              {
                matchCount > 0 &&
                <IonBadge>{matchCount < 99 ? matchCount : `99+`}</IonBadge>
              }
            </IonTabButton>
          }

          {
            eventToggles.direct_messaging &&
            <IonTabButton tab="inbox" href={`/home/inbox`}>
              <IonIcon icon={fileTrayFullOutline} />
              <IonLabel>Messages</IonLabel>
              {
                groupUnreadCount > 0 &&
                <IonBadge>{groupUnreadCount < 99 ? groupUnreadCount : `99+`}</IonBadge>
              }
            </IonTabButton>
          }

          <IonTabButton tab="users" href={`/home/users`}>
            <IonIcon icon={searchOutline} />
            <IonLabel>Search</IonLabel>
          </IonTabButton>

        </IonTabBar>
      </IonTabs>
      </IonPage>
    </Fragment>
    
  );
};

export default Home;
