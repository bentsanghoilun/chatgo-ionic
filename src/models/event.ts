export interface EventFilter {
    allow: boolean;
    type: "chat" | "email" | "ip";
    value: string;
    wildcard: boolean;
}