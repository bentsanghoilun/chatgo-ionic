import {
  IonApp,
  IonRouterOutlet
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';
import '@ionic/react/css/display.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/float-elements.css';
/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/typography.css';
import { useEffect, useState } from 'react';
import {
  Redirect,
  Route,
  Switch
} from 'react-router-dom';
import { boostrapFirebase } from './app/firebase';
import { useAppDispatch, useAppSelector } from './app/hooks';
import { selectEventName, selectIsAutoAuth, selectMode, setDmId, setEventName, setIsAutoAuth, setMode } from './app/slices/initDataSlice';
import { store } from './app/store';
import { InitData } from './app/types/InitData';
import AuthGuard from './components/AuthGuard';
import DmRouting from './components/DmRouting';
import UserMenu from './components/UserMenu';
import { isAuthenticated, selectEvent_key, setCredentials, setEvent_key } from './pages/Auth/authSlice';
import Login from './pages/Auth/Login';
import Register from './pages/Auth/Register';
import ChatRoomIonic from './pages/ChatRooms/ChatRoomIonic';
import VideoCall from './pages/VideoCall/VideoCall';
import GroupInfo from './pages/Groups/GroupInfo';
import Home from './pages/Home/Home';
import MatchmakingReminder from './pages/Home/MatchmakingReminder';
import DirectMessageIonic from './pages/Messages/DirectMessageIonic';
import BlockedUsers from './pages/Settings/BlockedUsers';
import MatchMakingSettings from './pages/Settings/MatchMakingSettings';
import NotificationsSettings from './pages/Settings/NotificationsSettings';
import ProfileSettings from './pages/Settings/ProfileSettings';
import ResetPassword from './pages/Settings/ResetPassword';
import UserProfile from './pages/Users/UserProfile';
import './theme/font.css';
import './theme/global.css';
import './theme/variables.css';

interface Props {
  initData: InitData;
}

const App: React.FC<Props> = (props: Props) => {
  const isAuth: boolean = useAppSelector(isAuthenticated);
  const isAutoAuth: boolean = useAppSelector(selectIsAutoAuth);
  const event_name: string | null | undefined = useAppSelector(selectEventName);
  const event_key: string | null | undefined = useAppSelector(selectEvent_key);
  const mode: string | null | undefined = useAppSelector(selectMode);
  const [viewportInfo, setViewportInfo] = useState({
    width: window.innerWidth,
    height: window.innerHeight
  });
  const [didDmRoute, setDidDmRoute] = useState(false);
  const [checkedMatchmaking, setCheckedMatchmaking] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setViewportInfo({
        width: window.innerWidth,
        height: window.innerHeight
      });
    }

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    }

  }, []);

  useEffect(() => {
    if (isAuth) {
      boostrapFirebase();
    }
  }, [isAuth]);

  const dispatch = useAppDispatch();
  useEffect(() => {
    props.initData.dmId && store.dispatch(setDmId(props.initData.dmId));

    props.initData.hasOwnProperty('isAutoAuth') && props.initData.isAutoAuth !== isAutoAuth && store.dispatch(setIsAutoAuth(props.initData.isAutoAuth));

    if (props.initData.eventKey && props.initData.eventKey !== '' && event_key === '') {
      dispatch(setEvent_key(props.initData.eventKey));
    }

    if (props.initData.userToken && props.initData.firebaseToken) {
      dispatch(setCredentials({
        access_token: props.initData.userToken,
        firebase_token: props.initData.firebaseToken
      }));
    }

    if (props.initData.event_name && props.initData.event_name !== event_name) {
      dispatch(setEventName(props.initData.event_name));
    }

    if (props.initData.mode && props.initData.mode !== mode) {
      dispatch(setMode(props.initData.mode));
    }

  }, [props.initData, dispatch, event_key, isAutoAuth, event_name, mode]);

  return (
    <IonApp>
      <IonReactRouter>
        <UserMenu />
        <IonRouterOutlet
          id="main"
          animated={true}
        >
          <AuthGuard checkedMatchmaking={checkedMatchmaking} >
            {
              !didDmRoute && <DmRouting setDidDmRoute={(bool: boolean) => setDidDmRoute(bool)} />
            }
            {/* <IonPage> */}
            <Switch>
              <Route exact path={`${process.env.PUBLIC_URL}/login`} render={() => <Login />} />
              <Route exact path={`${process.env.PUBLIC_URL}/register`} render={() => <Register />} />
              <Route exact path={`${process.env.PUBLIC_URL}/videoCall/:id`} render={() => <VideoCall isCaller={true} />} />
              <Route exact path={`${process.env.PUBLIC_URL}/videoCallReceived/:id`} render={() => <VideoCall />} />
              <Route path={`/home`}>
                <Home viewportInfo={viewportInfo} initData={props.initData} />
              </Route>
              <Route path={`${process.env.PUBLIC_URL}/matchmakingsettings`} render={() => <MatchMakingSettings />} exact={true} />
              <Route path={`${process.env.PUBLIC_URL}/notificationssettings`} render={() => <NotificationsSettings />} exact={true} />
              <Route path={`${process.env.PUBLIC_URL}/profilesettings`} render={() => <ProfileSettings />} exact={true} />
              <Route path={`${process.env.PUBLIC_URL}/resetpassword`} render={() => <ResetPassword />} exact={true} />
              <Route path={`${process.env.PUBLIC_URL}/blockedusers`} render={() => <BlockedUsers />} exact={true} />
              <Route path={`${process.env.PUBLIC_URL}/chatroom/:id`} render={() => <ChatRoomIonic />} />
              <Route path={`${process.env.PUBLIC_URL}/group/:id`} render={() => <DirectMessageIonic />} />
              <Route path={`${process.env.PUBLIC_URL}/userprofile/:id`} render={() => <UserProfile />} />
              <Route path={`${process.env.PUBLIC_URL}/groupinfo/:id`} render={() => <GroupInfo />} />
              <Route path={`/matchmaking-reminder`} render={() => <MatchmakingReminder setCheckedMatchmaking={(bool: boolean) => setCheckedMatchmaking(bool)} />} exact />
              <Route render={() => <Redirect to={`/home`} />}></Route>
            </Switch>
            {/* </IonPage> */}
          </AuthGuard>
        </IonRouterOutlet>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
