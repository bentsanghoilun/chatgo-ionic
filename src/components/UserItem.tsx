import { Fragment, useCallback, useState } from "react"
import { IonItem, useIonActionSheet, IonLabel, useIonAlert, IonLoading } from "@ionic/react";
import { ActionSheetButton } from '@ionic/core';
import { useHistory } from 'react-router';
import UserAvatar from "./UserAvatar";
import { useAppSelector } from "../app/hooks";
import { getDMGroup, userNotBlocked } from "../app/firebase";
import { useUserBlockMutation, useUserUnBlockMutation } from "../app/services/userApi";
import { selectEventToggles } from '../app/slices/eventSlice';
import { EventToggles } from '../app/types/eventToggles';
import UserInfoChecked from '../components/Classes/UserInfoChecked'
interface Props{
    id: number;
    index?: number;
    firstName: string;
    lastName: string;
    jobTitle?: string | null;
    organisation?: string | null;
    status?: string;
    profileImage?: string | null;
    children?: any;
    actionObj?: any;
    showActions?: boolean;
}

const UserItem: React.FC<Props> = (props: Props) => {
    const [present, dismiss] = useIonActionSheet();
    const history = useHistory();
    const showActions = props.hasOwnProperty('showActions') ? props.showActions : true;
    const [showDMLoader, setshowDMLoader] = useState(false);
    const [ block, { isBlockLoading } ] = useUserBlockMutation();
    const [unblock, { isUnBlockLoading }] = useUserUnBlockMutation();
    const eventToggles: EventToggles | never[] = useAppSelector(selectEventToggles);
    const [alert] = useIonAlert();

    const createAlertMessageBtn = useCallback(() => {
        let buttons: Array<ActionSheetButton> = [];
        if (userNotBlocked(String(props.id))) {
            buttons = [
                {
                    text: 'View Profile',
                    handler: () => history.push({
                        pathname: `/userprofile/${props.id}`,
                        state: {
                            id: props.id,
                            firstName: props.firstName,
                            lastName: props.lastName,
                            jobTitle: props.jobTitle,
                            organisation: props.organisation,
                            profilePicture: props.profileImage,
                            status: props.status
                        }
                    })
                },
                // { text: `Video Call ${props.firstName}` }
            ];
            eventToggles.direct_messaging && buttons.unshift({
                text: `Message ${props.firstName}`,
                handler: () => {
                    setshowDMLoader(true);
                    getDMGroup(props.id).then(id => {
                        setshowDMLoader(false);
                        setTimeout(() => history.push(`/group/${id}`), 300);
                    })
                }
            });
            buttons.push({ 
                text: `Block ${props.firstName}`, 
                handler: async () => {
                    const response: {data: { success: boolean } } = await block({
                        target: props.id,
                    })

                    var message = `Failed to block the ${props.firstName}`;
                    var header = "Error";
                    if (response.data.success) {
                        message = `Successfully blocked ${props.firstName}`;
                        header = "Success";
                    }

                    alert({
                        header,
                        message,
                        buttons: [
                            'OK',
                        ],
                    });
                }
            })
        } else {
            buttons.push({ 
                text: `Unblock ${props.firstName}`, 
                handler: async () => {
                    const response: {data: { success: boolean } } = await unblock({
                        target: props.id,
                    })

                    var message = `Failed to unblock the ${props.firstName}`;
                    var header = "Error";
                    if (response.data.success) {
                        message = `Successfully unblocked ${props.firstName}`;
                        header = "Success";
                    }

                    alert({
                        header,
                        message,
                        buttons: [
                            'OK',
                        ],
                    });
                }
            })
        }

        buttons.push({
            text: 'Cancel',
            role: 'cancel'
        })
    
        return buttons;
    }, [eventToggles, props, history, block, unblock, alert]);

    return (
        <Fragment>
            <IonItem
                key={props.index ? props.index : props.id}
                style={{ cursor: 'pointer' }}
                onClick={!showActions ? () => {} :
                    props.actionObj ? () => present(props.actionObj) :
                        () => present({ buttons: createAlertMessageBtn() })
                }
                className='ion-margin-bottom'
            >
                <UserAvatar
                    id={props.id}
                    firstName={props.firstName}
                    img={props.profileImage ? props.profileImage : null}
                    status={props.status}
                />
                <IonLabel>
                    <h2>{`${props.firstName} ${props.lastName}`}</h2>
                    <p><UserInfoChecked jobTitle={props.jobTitle} organisation={props.organisation} /></p>
                </IonLabel>
            </IonItem>
            <IonLoading
                isOpen={showDMLoader}
                message={'Loading Chat...'}
                onDidDismiss={() => setshowDMLoader(false)}
            />
        </Fragment>
    )
}

export default UserItem;