import { BrowserRouter } from "react-router-dom"

interface BrowserMessage{
    type: string;
    message?: any;
}

export const PostMessageToParent = async (message: BrowserMessage) => {
    window.parent.postMessage(message, "*");
}