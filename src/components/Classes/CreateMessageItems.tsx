import { Message, MessageDisplayItem } from '../../models/message';

const CreateMessageItems = (messages: Array<Message>) => {
    let items: Array<MessageDisplayItem> = [];

    const createItem = (type: 'divider' | 'message', message: Message) => {
        return {
            type: type,
            message: message
        };
    }

    messages.forEach((message, index) => {
        if (index === 0) {
            items.push(createItem('divider', message));
        // } else if (index === messages.length - 1) {
        } else {
            const showDivider =
                new Date(messages[index - 1].timestamp * 1000).getFullYear() !==
                    new Date(messages[index].timestamp * 1000).getFullYear() ? true :
                    new Date(messages[index - 1].timestamp * 1000).getMonth() !==
                        new Date(messages[index].timestamp * 1000).getMonth() ? true :
                        new Date(messages[index - 1].timestamp * 1000).getDate() !==
                            new Date(messages[index].timestamp * 1000).getDate() ? true : false
            
            showDivider &&
                items.push(createItem('divider', message));
        }
        items.push(createItem('message', message));
    });

    //return items array
    return items;
}

export default CreateMessageItems;