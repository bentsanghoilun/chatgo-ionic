import { IonLoading } from '@ionic/react';
import { Fragment, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { getDMGroup } from '../app/firebase';
import { useAppSelector } from '../app/hooks';
import { selectDmId } from '../app/slices/initDataSlice';
import { userIdSelector } from '../app/slices/userSlice';

interface Props {
    setDidDmRoute: (bool: boolean) => void;
}

const DmRouting: React.FC<Props> = (props: Props) => {
    const userId = useAppSelector(userIdSelector);
    const history = useHistory();
    const dmId = useAppSelector(selectDmId);
    const [showAlert, setShowAlert] = useState(false);

    // add a user effect to check if props changed
    useEffect(() => {
        if (dmId && userId !== 0) {
            if (parseInt(dmId) === userId) {
                props.setDidDmRoute(true);
                history.push(`/inbox`);
            } else {
                setShowAlert(true);
                getDMGroup(parseInt(dmId)).then((id) => {
                    setShowAlert(false);
                    setTimeout(() => {
                        if (id !== '') {
                            props.setDidDmRoute(true);
                            history.push(`${process.env.PUBLIC_URL}/group/${id}`);
                        }
                    }, 300);
                });
            }
        }
    }, [dmId, userId]);

    return <Fragment>
        <IonLoading
            isOpen={showAlert}
            message={'Loading Chat...'}
        />
    </Fragment>;
};

export default DmRouting;