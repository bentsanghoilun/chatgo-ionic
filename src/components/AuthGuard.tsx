import { useEffect, useState } from 'react';
import { Redirect, useHistory, useLocation } from "react-router";
import { useAppSelector, useVideoCallWatcher } from "../app/hooks";
import { selectEventToggles } from '../app/slices/eventSlice';
import { selectDmId } from '../app/slices/initDataSlice';
import { selectUserId, selectUserMatchmaking } from "../app/slices/userSlice";
import { EventToggles } from '../app/types/eventToggles';
import { isAuthenticated } from "../pages/Auth/authSlice";
import { roomsIsInitialised, selectAllRooms, selectLastMessages } from "../app/slices/roomSlice";
import { selectQuestions } from "../app/slices/questionSlice";
import { Question } from "../app/types/questions";

interface Props{
  children: any;
  checkedMatchmaking?: boolean;
}

const AuthGuard: React.FC<Props> = (props:Props) => {
  const isAuth: boolean = useAppSelector(isAuthenticated);
  const location = useLocation();
  const history = useHistory();
  useVideoCallWatcher()
  const dmId = useAppSelector(selectDmId);
  const userId = useAppSelector(selectUserId);
  const matchmaking = useAppSelector(selectUserMatchmaking);
  const [checkedMatchmaking, setCheckedMatchmaking] = useState(props.checkedMatchmaking); 
  const eventToggles: EventToggles | never[] = useAppSelector(selectEventToggles);
  const initRooms = useAppSelector(selectAllRooms);
  const IsRoomInitialised = useAppSelector(roomsIsInitialised);
  const questions: Array<Question> = useAppSelector(selectQuestions);
  const [homeRoute, setHomeRoute] = useState('/chatrooms');
  const [chatRoomFirstLanding, setChatRoomFirstLanding] = useState(true);

  useEffect(() => {
    var tempHomeRoute = homeRoute;
    if (Object.keys(eventToggles).length === 0) { return };

    if (eventToggles.chat_rooms) {
      if (IsRoomInitialised && initRooms.length === 1 && chatRoomFirstLanding) {
        tempHomeRoute = `/chatroom/${initRooms[0].id}`
        history.push(tempHomeRoute);
        chatRoomFirstLanding && setChatRoomFirstLanding(false);
      }
    } else {
      if (eventToggles.direct_messaging) {
        tempHomeRoute = '/inbox';
      } else if(eventToggles.matchmaking) {
        tempHomeRoute = '/matches';
      } else {
        tempHomeRoute = '/users';
      }
    }

    if (tempHomeRoute !== homeRoute) {
      setHomeRoute(tempHomeRoute)
    }
  }, [eventToggles, homeRoute, IsRoomInitialised, initRooms, chatRoomFirstLanding, history])

  useEffect(() => {
    setCheckedMatchmaking(props.checkedMatchmaking);
  }, [props.checkedMatchmaking]);

  useEffect(() => {
    if (isAuth === true && !dmId && !matchmaking && userId !== 0 && !checkedMatchmaking && eventToggles.matchmaking && questions && questions.length > 0) {
      history.push('/matchmaking-reminder');
    }
  }, [isAuth, dmId, matchmaking, userId, checkedMatchmaking, history, eventToggles, questions]);

  if (isAuth === true) {
    return (
      location.pathname === `${process.env.PUBLIC_URL}/login` || location.pathname === `${process.env.PUBLIC_URL}/register` ? (
        <Redirect
          to={{
            pathname: `/home${homeRoute}`,
            state: { from: location.pathname }
          }}
        />
      ) : (props.children)
    )
  } else {
    return (
      location.pathname === `${process.env.PUBLIC_URL}/login` || location.pathname === `${process.env.PUBLIC_URL}/register` ? (
        props.children
      ) : (
        <Redirect
          to={{
            pathname: `${process.env.PUBLIC_URL}/login`,
            state: { from: location.pathname }
          }}
        />
      )
    )
  }    
}

export default AuthGuard;