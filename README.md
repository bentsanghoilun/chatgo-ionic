# Chatgo Ionic

## Requirements

## Getting started
Run the following commands inside the project folder to setup and run the app locally.

```
npm install @ionic/cli -g
npm install
cp .env.example .env
```

You will need to add a chatGo event key and API endpoint to your .env file before running the app.

## Web
```
ionic serve
```

## Android
Ensure you have Android Studio with Android API version 30 installed and ensure you have `JAVA_HOME` pointing to JDK and `ANDROID_SDK_ROOT` pointing to Android SDK in your system variables.

```
ionic capacitor add android
ionic cap run android -l --external
```

## IOS
On Mac OS ensure you have xcode installed and run the following commands

```
xcode-select --install
ionic capacitor add ios
ionic cap run ios -l --external
```

## Widget
To compile the chatGo.js widget.
```
npm run build-widget
```

This can then be used in web pages for embedding;
```
<script src='chatGo.js'></script>
<script>
    window.addEventListener('DOMContentLoaded', function(){
        chatGo.init( 'EVENT_KEY', {
            mode: 'embed|widget',
            embedId: 'ELEMENT_ID', // When used as direct embed
            openOnloaded: true,
            dmId: USER_ID, // For use when embedded as a direct chat to a specific user
            widgetPosition: 'start top',
            desktopEnabled: true
        });
    });
</script>
```
